; Copyright 2000 Pace Micro Technology plc
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; Assembler function to enable podule irqs

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Proc
        GET     Hdr:EnvNumbers
        GET     Hdr:ModHand

        EXPORT  MICRODELAY
        EXPORT  disable_irqs
        EXPORT  enable_irqs
        EXPORT  splet
        EXPORT  splimp
        EXPORT  splnet
        EXPORT  splx
        EXPORT  set_irqsareoff
        EXPORT  clear_irqsareoff
        EXPORT  et_irq_entry1

        ^       0

        AREA    poduleirqs,PIC,CODE,REL

        IMPORT  et_irq_entry
et_irq_entry1
        LDR     R0,[R12,#4]       ; unit
        LDR     R12,[R12,#12]     ; wsp
        B       et_irq_entry

irqsareoff
        DCD     0

;
;    **********************************************************************
;

        EXPORT  MICRODELAY

; void MICRODELAY(int n, u_char *regs)
;
;       implement an n-microsecond delay: a medium cycle read of the ECID
;       register takes 6 x 8MHz clock ticks, regardless of how fast the
;       processor is.
;       So take 8/6 of 'n' - this is a *minimum* delay of n microseconds.

MICRODELAY  ROUT
        MOV     r2, #43                 ; 1/6 in 8 bit fixed point
        MUL     r0, r2, r0
        MOVS    r0, r0, LSR #8-3        ; Approx 8/6 of 'n'
        MOVEQ   pc, lr
10
        LDRB    r3, [r2, #0]            ; ECID
        SUBS    r0, r0, #1
        BNE     %BT10

        MOV     pc, lr

;
;       ************************************************************
;

        IMPORT  |_Mod$Reloc$Off|
        IMPORT  ioc_base
ioc_base_ptr
        DCD     ioc_base                 ;Relocated address of ioc_base from C

; os_error *enable_podule_irqs();
enable_podule_irqs ROUT
        Entry  "r0-r1"

        SavePSR R12                      ;Hold current I_bit & F_bit & Mode
        WritePSRc I_bit+F_bit+SVC_mode,R3 ;Disable IRQs & FIQs
        LDR     R0, ioc_base_ptr         ;whilst enabling Podule IRQs in IOC
        DCD     &E51AE000 + |_Mod$Reloc$Off|
        LDR     R0,[R0,R14]              ;Relocate and read IOC base
        LDRB    R1,[R0,#IOCIRQMSKB]
        ORR     R1,R1,#podule_IRQ_bit
        STRB    R1,[R0,#IOCIRQMSKB]
        RestPSR R12,,cf                  ;Restore original I/F bits & Mode.

        ; Exit with no recovery
        EXIT

; os_error *disable_podule_irqs();
disable_podule_irqs ROUT
        Entry  "r0-r1"

        SavePSR R12                      ;Hold current I_bit & F_bit & Mode
        WritePSRc I_bit+F_bit+SVC_mode,R3 ;Disable IRQs & FIQs
        LDR     R0, ioc_base_ptr         ;whilst enabling Podule IRQs in IOC
        DCD     &E51AE000 + |_Mod$Reloc$Off|
        LDR     R0,[R0,R14]              ;Relocate and read IOC base
        LDRB    R1,[R0,#IOCIRQMSKB]
        BIC     R1,R1,#podule_IRQ_bit
        STRB    R1,[R0,#IOCIRQMSKB]
        RestPSR R12,,cf                  ;Restore original I/F bits & Mode.

        ; Exit with no recovery
        EXIT

;int  disable_irqs();
; returns:
;         1 - If IRQs were already disabled.
;         0 - If this call disabled IRQs.
;
disable_irqs ROUT
splet
splnet
splimp
        LDR    r0,irqsareoff
        MOVS   r0,r0
        MOVNE  pc,lr
        B      disable_podule_irqs

;void  enable_irqs();
;  irqsareoff
;         1 - Just return.
;         0 - Enable podule IRQs.
;
enable_irqs ROUT
splx
        LDR   r0,irqsareoff
        MOVS  r0,r0
        MOVNE pc,lr                 ;If so, return.
        B     enable_podule_irqs

set_irqsareoff
        MOV   r0,#1
        STR   r0,irqsareoff
        MOV   pc,lr

clear_irqsareoff
        MOV   r0,#0
        STR   r0,irqsareoff
        MOV   pc,lr

        END

